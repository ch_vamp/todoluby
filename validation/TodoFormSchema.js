import * as Yup from 'yup';
 
const collectionName = 'TodoLubyCollection';


const TodoFormSchema = Yup.object().shape({
  tarefa: Yup.string()
    .min(5, 'A tarefa deve conter pelo menos cinco caracteres!')
    .required('Infome uma tarefa!')
    .test('unique', 'Esta tarefa já consta na lista!', (value) => {
      const collection = localStorage.getItem(collectionName);
      let arraTarefas;
      if (collection)
        arraTarefas = JSON.parse(collection);

      if (Array.isArray(arraTarefas) && arraTarefas.includes(value))
        return false;
      else
        return true;
    })
});

export default TodoFormSchema;