import Head from 'next/head';

import TodoForm from '../components/TodoForm';
import TodoLista from '../components/TodoLista';

const collectionName = 'TodoLubyCollection';

export default function Home() {
  return (
    <div className="container">
      <Head>
        <title>TodoLuby</title>
        <link rel="icon" href="/favicon.ico" />
        <link href="/geral.css" rel="stylesheet" />
      </Head>
      <div className='main'> 
        <h1 className='nomeProjeto'>
          Luby Todo
        </h1>
        {/*<TodoForm />*/}
        <TodoForm  collectionName={collectionName}/>
        <TodoLista collectionName={collectionName}/>
      </div>  
    </div>
  )
}
