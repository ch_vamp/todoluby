import React from 'react';
import styled from 'styled-components'


const Div = styled.div`
  width: 100%;
  margin: auto;
  grid-column: 2;
`;

class TodoLista extends React.Component {
  constructor(props) {
    super(props);
    this.state = {tarefas: []};
    this.btnListarTarefasOnClick = this.btnListarTarefasOnClick.bind(this);
    this.createListItens = this.createListItens.bind(this);
  }
  createListItens () {
    return this.state.tarefas.map(tarefa => {
      return <li key={tarefa}>{tarefa}</li>;
    })
  }
  btnListarTarefasOnClick () {
    const collection = localStorage.getItem(this.props.collectionName);
    let arrTarefas = [];
    if (collection)
      arrTarefas = JSON.parse(collection);
    
    if (!Array.isArray(arrTarefas))
      throw new Error('Erro de schema na "TodoLubyCollection"');

    this.setState({tarefas:arrTarefas});
  }
  render () {
    return (
      <Div id='todoList-conteiner'>
        <h2>Tarefas cadastradas</h2>
        <button className='btnPrimary' id='btnListarTarefas' onClick={this.btnListarTarefasOnClick}>
          Listar
        </button>
        <ol id='todoList-ul'>{this.createListItens()}</ol>    
      </Div>
    )
  }
}

export default TodoLista;