import React from 'react';
import styled from 'styled-components'
import { Formik, Field, ErrorMessage, Form } from 'formik';
 

import TodoFormSchema from '../validation/TodoFormSchema';

const FormConteiner = styled.div`
  width: 100%;
  margin: auto;
  grid-column: 2;
`;

const TodoForm2 = (props) => {
  return (
    <FormConteiner>
      <Formik
        initialValues={{ tarefa: ''}}
        validationSchema={TodoFormSchema}
        onSubmit={(values, { setSubmitting, resetForm}) => {
          const collection = localStorage.getItem(props.collectionName);
          let arrTarefas;
          let novoArrTarefas = [];
          if (collection) {
            arrTarefas = JSON.parse(collection);
          } 
          if (Array.isArray(arrTarefas))
            novoArrTarefas = [...arrTarefas, values.tarefa]
          else
            novoArrTarefas = [values.tarefa];

          localStorage.setItem(props.collectionName, JSON.stringify(novoArrTarefas));
          setSubmitting(false);
          resetForm();
        }}
     >
        {({ isSubmitting }) => (
          <Form>
            <label>Tarefa </label>
            <Field type="text" name="tarefa" />
            <ErrorMessage name="tarefa" component="div" className='errorMsg'/>
            <button type="submit" disabled={isSubmitting} className='btnPrimary'>
              Adicionar
            </button>
          </Form>
        )}
      </Formik>
    </FormConteiner>
  )
};

export default TodoForm2;